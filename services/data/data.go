package data

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"shopx/training/weatherApp/model"
	model_api "shopx/training/weatherApp/model/api/external"
)

func GetForeCastFromAPI(client *http.Client, city string) (model.Forecast, error) {
	jsonData, err := GetForecastByCityJSON(client, city)
	if err != nil {
		return model.Forecast{}, err
	}

	apiResp, err := JsonToModelForecast(jsonData)
	if err != nil {
		return model.Forecast{}, err
	}

	return apiRespToModel(apiResp)
}

func GetForecastByCityJSON(client *http.Client, city string) (string, error) {
	// create http Get request
	req, err := http.NewRequest(http.MethodGet, "https://api.openweathermap.org/data/2.5/weather", nil)
	if err != nil {
		log.Fatal(err)
	}

	// set API key in request header
	params := req.URL.Query()
	params.Add("q", city) // city name value
	params.Add("appid", "2e4e954780c338109e7bcd85cdd8472a")
	req.URL.RawQuery = params.Encode()
	resp, err := client.Do(req)

	if err != nil {
		return "", errors.New("Fail to retrieve data")
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return "", errors.New("Fail to read retrieve data")
	}

	return string(body), nil
}

func JsonToModelForecast(jsonData string) (model_api.ForeCastAPIResponse, error) {
	var x model_api.ForeCastAPIResponse
	if err := json.Unmarshal([]byte(jsonData), &x); err != nil {
		return model_api.ForeCastAPIResponse{}, err
	}

	return x, nil

}

func apiRespToModel(resp model_api.ForeCastAPIResponse) (model.Forecast, error) {
	return model.Forecast{
		City:      resp.Name,
		Main:      resp.Weather[0].Main,
		Temp:      resp.Main.Temp,
		TempMin:   resp.Main.TempMin,
		TempMax:   resp.Main.TempMax,
		Pressure:  resp.Main.Pressure,
		Humidity:  resp.Main.Humidity,
		WindSpeed: resp.Wind.Speed,
	}, nil
}
