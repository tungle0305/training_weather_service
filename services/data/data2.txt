{
    "coord":{"lon":139.6917,"lat":35.6895},
    "weather":[{"id":801,"main":"Clouds","description":"few clouds","icon":"02d"}],
    "base":"stations",
    "main":{"temp":279.96,"feels_like":277.22,"temp_min":278.03,"temp_max":281.23,"pressure":1019,"humidity":22},
    "visibility":10000,
    "wind":{"speed":4.02,"deg":319,"gust":5.36},
    "clouds":{"all":20},
    "dt":1642745895,
    "sys":{"type":2,"id":2001249,"country":"JP","sunrise":1642715299,"sunset":1642751765},
    "timezone":32400,
    "id":1850144,
    "name":"Tokyo",
    "cod":200
}