package data

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"testing"

	api_model "shopx/training/weatherApp/model/api/external"
	"shopx/training/weatherApp/services/util"
)

func TestGetData(t *testing.T) {
	client := &http.Client{}

	jsonData, err := GetForecastByCityJSON(client, "Istanbul")
	if err != nil {
		t.Error(err)
	}

	log.Println("JSON: " + jsonData)
	modelData, err := JsonToModelForecast(jsonData)
	if err != nil {
		t.Error(err)
	}
	log.Println(modelData)
}

func test1(t *testing.T) {
	f, _ := ioutil.ReadFile("data2.txt")
	jsonData := string(f)
	var x api_model.ForeCastAPIResponse

	err := json.Unmarshal([]byte(jsonData), &x)
	if err != nil {
		t.Errorf("%v", err)
	}
	log.Println(x)

}

type A struct {
	Main B `json:"main"`
}

type B struct {
	Temp       float32 `json:"temp"`
	Feels_like float32 `json:"feels_like"`
}

func testForeCastModel(t *testing.T) {
	client := &http.Client{}

	json, err := GetForecastByCityJSON(client, "Tokyo")
	if err != nil {
		util.Check(err, "get data from ext API")
	}

	log.Println(json)
	log.Println("##########")
	log.Println(JsonToModelForecast(json))
}

func testUnmarshal(t *testing.T) {
	var x A
	data := `{"main":{"temp":279.96,"feels_like":277.22}}`

	json.Unmarshal([]byte(data), &x)
	log.Println(data)
	log.Println(x)
}
