package util

import "os"

func SetOSEnv() {
	os.Setenv("MONGO_URI", "mongodb://admin:password@"+os.Getenv("MONGO_IP")+":27017/test?authSource=admin")
	os.Setenv("MONGO_DATABASE", "weather_api")
	os.Setenv("SECRET", "secret")
}

func Check(err error, cause string) {
	if err != nil {
		panic("error at " + cause)
	}

}
