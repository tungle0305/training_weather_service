package db

import (
	"os"
	"testing"
)

func init() {
	os.Setenv("MONGO_URI", "mongodb://admin:password@localhost:27017/test?authSource=admin")
	os.Setenv("MONGO_DATABASE", "weather_api")
}

func TestMongoClient(t *testing.T) {
	client, err := NewMongoClient(os.Getenv("MONGO_URI"))
	if err != nil {
		t.Error(err)
	}

	_, err1 := NewMongoColl(client, "user")
	if err1 != nil {
		t.Error(err)
	}
}
