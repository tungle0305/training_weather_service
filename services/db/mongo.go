package db

import (
	"context"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func NewMongoClient(URI string) (*mongo.Client, error) {
	ctx := context.Background()

	return mongo.Connect(ctx, options.Client().ApplyURI(URI))

}

func NewMongoColl(Client *mongo.Client, name string) (*mongo.Collection, error) {
	if err := Client.Ping(context.TODO(),
		readpref.Primary()); err != nil {
		return &mongo.Collection{}, err
	}

	Coll := Client.Database(os.Getenv("MONGO_DATABASE")).Collection(name)

	return Coll, nil
}
