package auth

import "testing"

var service JWTService

func init() {
	service = NewJWTAuthService()
}

func TestGenAndValidateToken(t *testing.T) {
	encodedToken := service.GenerateToken("a1", true)
	token, _ := service.ValidateToken(encodedToken)

	if !token.Valid {
		t.Error("Fail to gen and validate token")
	}
}
