package validator

var validCities []string = []string{"London", "Bangkok", "New York", "HCM", "Sapa"}

func ValidateCityName(name string) bool {
	for _, city := range validCities {
		if city == name {
			return false
		}
	}

	return true
}
