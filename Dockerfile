# build stage
FROM golang:1.17.6-alpine3.15 AS builder
RUN mkdir /app
WORKDIR /app
COPY . .
RUN go build -o main .

# run stage
FROM alpine:3.15
WORKDIR /app
COPY --from=builder /app/main . 


EXPOSE 8080
ENTRYPOINT [ "/app/main" ]