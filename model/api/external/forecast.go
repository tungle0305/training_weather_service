package model

type ForeCastAPIResponse struct {
	Coord    coord     `json:"coord"`
	Weather  []weather `json:"weather"`
	Base     string    `json:"base"`
	Main     main      `json:"main"`
	Wind     wind      `json:"wind"`
	Clouds   clouds    `json:"clouds"`
	Rain     rain      `json:"rain"`
	Snow     snow      `json:"snow"`
	Dt       dt        `json:"dt"`
	Sys      sys       `json:"sys"`
	Timezone int32     `json:"timezone"`
	Id       int32     `json:"id"`
	Name     string    `json:"name"`
	Cod      int32     `json:"cod"`
}

type coord struct {
	Lon float32 `json:"lon"`
	Lat float32 `json:"lat"`
}

type weather struct {
	Id          int    `json:"id"`
	Main        string `json:"main"`
	Description string `json:"description"`
	Icon        string `json:"icon"`
}

type main struct {
	Temp      float32 `json:"temp"`
	FeelsLike float32 `json:"feels_like"`
	Pressure  float32 `json:"pressure"`
	Humidity  float32 `json:"humidity"`
	TempMin   float32 `json:"temp_min"`
	TempMax   float32 `json:"temp_max"`
	SeaLevel  float32 `json:"sea_level"`
	GrndLevel float32 `json:"grnd_level"`
}

type wind struct {
	Speed float32 `json:"speed"`
	Deg   float32 `json:"deg"`
	Gust  float32 `json:"gust"`
}

type clouds struct {
	All float32 `json:"all"`
}

type rain struct {
	Vol1h float32 `json:"1h"`
	Vol3h float32 `json:"3h"`
}

type snow struct {
	Vol1h float32 `json:"1h"`
	Vol3h float32 `json:"3h"`
}

type dt int32

type sys struct {
	Type    int32  `json:"type"`
	Id      int32  `json:"id"`
	Message string `json:"message"`
	Country string `json:"country"`
	Sunrise int32  `json:"sunrise"`
	Sunset  int32  `json:"sunset"`
}
