package model

import "fmt"

type Forecast struct {
	City      string  `json:"city" bson:"city"`
	Main      string  `json:"main" bson:"main"`
	Temp      float32 `json:"temp" bson:"temp"`
	TempMin   float32 `json:"temp_min" bson:"temp_min"`
	TempMax   float32 `json:"temp_max" bson:"temp_max"`
	Pressure  float32 `json:"pressure" bson:"pressure"`
	Humidity  float32 `json:"humidity" bson:"humidity"`
	WindSpeed float32 `json:"wind_speed" bson:"wind_speed"`
}

func (f Forecast) ToString() string {
	return fmt.Sprintf("{city: %s, main: %s, temp: %f, temp_min: %f, temp_max: %f, pressure: %f, humidity: %f, wind_speed: %f}",
		f.City, f.Main, f.Temp, f.TempMin, f.TempMax, f.Pressure, f.Humidity, f.WindSpeed)
}
