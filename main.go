package main

import (
	"net/http"
	"shopx/training/weatherApp/controller/middleware"
	"shopx/training/weatherApp/model"
	repo_forecast "shopx/training/weatherApp/repo/city"
	repo_user "shopx/training/weatherApp/repo/user"
	auth "shopx/training/weatherApp/services/auth"
	"shopx/training/weatherApp/services/data"
	"shopx/training/weatherApp/services/util"
	forecast "shopx/training/weatherApp/usecase/forecast/by/city"
	usecase "shopx/training/weatherApp/usecase/forecast/by/city"

	"github.com/gin-gonic/gin"
)

var httpClient *http.Client
var router *gin.Engine
var authorized *gin.RouterGroup
var foreCastUcase forecast.ConcreteForecastByCityUseCase
var foreCastRepo repo_forecast.ConcreteForecastRepo
var userRepo repo_user.ConcreteUserRepo
var authService auth.JWTService

func init() {
	util.SetOSEnv()
	var err error
	authService = auth.NewJWTAuthService()

	userRepo, err = repo_user.NewConcreteUserRepo()
	util.Check(err, "init user repo")

	foreCastRepo, err = repo_forecast.NewForecastRepo()
	util.Check(err, "init forecast repo")

	foreCastUcase = forecast.ConcreteForecastByCityUseCase{
		ForecastRepo: foreCastRepo,
		AuthService:  authService,
	}

	httpClient = &http.Client{}
}

func signUpHandler(c *gin.Context) {
	var user model.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	name := user.Name
	pass := user.Password

	if name == "" || pass == "" {
		c.AbortWithStatus(http.StatusBadRequest)
	}

	c.JSON(200, gin.H{"hello": name})
	err := userRepo.NewRecord(name, "", pass)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}

func loginHandler(c *gin.Context) {
	var user model.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if !userRepo.IsValid(user.Name, user.Password) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid username or password"})
		return
	}

	tokenString := authService.GenerateToken(user.Name, true)
	c.JSON(200, gin.H{"Token": tokenString})
}

func forecastByCityHandler(c *gin.Context) {

	var req usecase.ForecastByCityRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	city := req.City

	if city == "" {
		c.AbortWithStatus(http.StatusBadRequest)
	}

	resp, err := foreCastUcase.Handle(forecast.ForecastByCityRequest{City: city})

	if err == nil {
		c.String(http.StatusOK, resp.Forecast.ToString())
	} else {
		modelData, err := data.GetForeCastFromAPI(httpClient, city)
		if err != nil {
			c.String(http.StatusNotFound, "Fail to retrieve data")
		}

		c.JSON(200, modelData)
	}
}

func indexHandler(c *gin.Context) {
	c.String(http.StatusOK, `Welcome`)
}

func main() {

	router := gin.Default()

	router.POST("/signup", signUpHandler)
	router.POST("/login", loginHandler)
	router.GET("/", indexHandler)
	router.GET("/forecast/by/city", middleware.AuthJWTMiddleware, forecastByCityHandler)

	router.Run(":8080")
}
