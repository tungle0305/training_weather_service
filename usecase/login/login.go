package usecase

import (
	"errors"
	"shopx/training/weatherApp/services/auth"
)

type LoginUseCase interface {
	Handle(LoginRequest) (LoginResponse, error) // input message are pure structs
}

type SignupUseCase interface {
	Handle(SignupRequest) (SignupResponse, error) // input message are pure structs
}

// ######################

type UserRepo interface {
	NewRecord(name, email, pass string) error
	IsExist(name string) bool
	IsValid(name, pass string) bool
	Delete(name string) error
}

type SignupRequest struct {
	Name     string `form:"name" json:"name" xml:"name"  binding:"required"`
	Email    string `form:"email" json:"email" xml:"email"  binding:"required"`
	Password string `form:"pass" json:"pass" xml:"pass"  binding:"required"`
}

type SignupResponse struct {
	IsSuccess bool
}

type ConcreteSignupUseCase struct {
	UserRepo UserRepo
}

func (u *ConcreteSignupUseCase) Handle(req SignupRequest) (SignupResponse, error) {
	if u.UserRepo.IsExist(req.Name) {
		return SignupResponse{IsSuccess: false}, errors.New("Account name already exist")
	}

	u.UserRepo.NewRecord(req.Name, req.Email, req.Password)
	return SignupResponse{IsSuccess: true}, nil
}

// #############################

type LoginRequest struct {
	Name     string `form:"name" json:"name" xml:"name"  binding:"required"`
	Password string `form:"pass" json:"pass" xml:"pass"  binding:"required"`
}

type LoginResponse struct {
	Token string
}

type ConcreteLoginUseCase struct {
	UserRepo UserRepo
}

func NewConcreteLoginUseCase(repo UserRepo) ConcreteLoginUseCase {
	return ConcreteLoginUseCase{UserRepo: repo}
}

func (u *ConcreteLoginUseCase) Handle(req LoginRequest) (LoginResponse, error) {
	if !u.UserRepo.IsValid(req.Name, req.Password) {
		return LoginResponse{Token: ""}, errors.New("Wrong name or password")
	}

	return LoginResponse{Token: auth.NewJWTAuthService().GenerateToken(req.Name, true)}, nil
}
