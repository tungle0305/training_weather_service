package usecase

import (
	repo "shopx/training/weatherApp/repo/user"
	"shopx/training/weatherApp/services/util"
	"testing"
)

var loginUcase ConcreteLoginUseCase
var signupUcase ConcreteSignupUseCase
var repoImp repo.ConcreteUserRepo

func init() {
	util.SetOSEnv()
	repoImp, _ = repo.NewConcreteUserRepo()
	loginUcase = ConcreteLoginUseCase{UserRepo: &repoImp}
	signupUcase = ConcreteSignupUseCase{UserRepo: &repoImp}
}

func testSignUp(t *testing.T) {
	name := "Ron"
	req := SignupRequest{Name: name, Email: "abc@gmail.com", Password: "abc"}
	_, err := signupUcase.Handle(req)
	if err != nil {
		t.Errorf("Fail to hanlde new sign up req")
	}

	_, err = signupUcase.Handle(req)
	if err == nil {
		t.Errorf("Fail to hanlde duplicated sign up req")
	}

}

func TestLogin(t *testing.T) {
	pass := "abc"
	name := "Lee"
	repoImp.NewRecord(name, "sth@gmail.com", pass)

	req := LoginRequest{Name: name, Password: pass}
	_, err := loginUcase.Handle(req)
	if err != nil {
		t.Errorf("Fail to hanlde correct log in")
	}

	req.Password = "bcd"
	_, err = loginUcase.Handle(req)
	if err == nil {
		t.Errorf("Fail to hanlde incorrect login")
	}

}
