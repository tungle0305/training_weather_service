package usecase

import (
	"log"
	"shopx/training/weatherApp/model"
	repo "shopx/training/weatherApp/repo/city"
	"shopx/training/weatherApp/services/auth"
	"shopx/training/weatherApp/services/util"
	"testing"
)

var forecastRepoImp repo.ConcreteForecastRepo
var foreCastUcase ConcreteForecastByCityUseCase
var authService auth.JWTService

func init() {
	util.SetOSEnv()
	forecastRepoImp, _ = repo.NewForecastRepo()
	authService = auth.NewJWTAuthService()
	foreCastUcase = ConcreteForecastByCityUseCase{ForecastRepo: forecastRepoImp, AuthService: authService}
}

func testBasicHandle(t *testing.T) {
	city := "New York"
	err := forecastRepoImp.NewRecord(model.Forecast{City: city, Main: "Snowy", Temp: 10.0})
	if err != nil {
		t.Errorf("Fail to insert new weather data")
	}

}

func Test1(t *testing.T) {
	city := "Tokyo"
	resp, err := forecastRepoImp.GetByCity(city)
	log.Println(err)
	log.Println(resp)

}
