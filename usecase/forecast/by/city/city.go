package usecase

import (
	"shopx/training/weatherApp/model"
	repo "shopx/training/weatherApp/repo/city"
	"shopx/training/weatherApp/services/auth"
)

type ForecastByCityUseCase interface {
	Handle(ForecastByCityRequest) (model.Forecast, error) // input,output message are pure structs
}

type ForecastByCityRequest struct {
	City string `form:"city" json:"city" xml:"city"  binding:"required"`
}

type ForecastResponse struct {
	Forecast model.Forecast
}

type ForecastRepo interface {
	GetByCity(cityName string) (model.Forecast, error)
	IsExist(cityName string) bool
	NewRecord(data model.Forecast) error
	Delete(cityName string) error
}

type ConcreteForecastByCityUseCase struct {
	ForecastRepo repo.ConcreteForecastRepo
	AuthService  auth.JWTService
}

func (u ConcreteForecastByCityUseCase) Handle(req ForecastByCityRequest) (ForecastResponse, error) {
	if !u.ForecastRepo.IsExist(req.City) {

	}

	data, err := u.ForecastRepo.GetByCity(req.City)

	return ForecastResponse{Forecast: data}, err
}
