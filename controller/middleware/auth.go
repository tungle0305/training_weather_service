package middleware

import (
	"fmt"
	"net/http"
	"shopx/training/weatherApp/services/auth"

	"github.com/gin-gonic/gin"
)

func AuthJWTMiddleware(c *gin.Context) {
	const BEARER_SCHEMA = "Bearer "
	authHeader := c.GetHeader("Authorization")
	tokenString := authHeader[len(BEARER_SCHEMA):]
	token, err := auth.NewJWTAuthService().ValidateToken(tokenString)
	if token.Valid {
		c.String(http.StatusOK, "Access granted\n")
	} else {
		fmt.Println(err)
		c.String(http.StatusOK, "Access denied\n")
		c.AbortWithStatus(http.StatusUnauthorized)
	}
}
