package repo

import (
	"log"
	"shopx/training/weatherApp/services/util"
	"testing"
)

var userRepo ConcreteUserRepo

func init() {
	util.SetOSEnv()
}

func TestInitRepo(t *testing.T) {
	_, err := NewConcreteUserRepo()

	if err != nil {
		t.Errorf("fail to init user repo %v", err)
	}
	log.Println(1)
}

func testNewRecord(t *testing.T) {
	userRepo, _ := NewConcreteUserRepo()

	err := userRepo.NewRecord("LEE", "sth@gmail.com", "123")
	if err != nil {
		t.Errorf("fail to insert new record")
	}

	err = userRepo.NewRecord("LEE", "sth@gmail.com", "123")
	if err == nil {
		t.Errorf("duplicate user account got inserted")
	}
}

func testIsExist(t *testing.T) {
	userRepo, _ := NewConcreteUserRepo()

	if !userRepo.IsExist("Tung") {
		t.Errorf("fail to check existed record")
	}

	if userRepo.IsExist("DUONG") {
		t.Errorf("fail to check non-existed record")
	}

}

func testDelete(t *testing.T) {
	userRepo, _ := NewConcreteUserRepo()

	if err := userRepo.Delete("Tung"); err != nil {
		t.Errorf("fail to delete existing record")
	}

	if err := userRepo.Delete("LONG"); err == nil {
		t.Errorf("fail to not-delete non-existed record")
	}

}

func TestValid(t *testing.T) {
	userRepo, _ = NewConcreteUserRepo()

	userRepo.NewRecord("Alan", "sth@gmail.com", "123")

	if !userRepo.IsValid("Alan", "123") {
		t.Errorf("fail to authenticate valid account")
	}

}
