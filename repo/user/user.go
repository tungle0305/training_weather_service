package repo

import (
	"context"
	"errors"
	"os"
	"shopx/training/weatherApp/model"
	"shopx/training/weatherApp/services/db"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type ConcreteUserRepo struct {
	client   *mongo.Client
	userColl *mongo.Collection
}

func NewConcreteUserRepo() (ConcreteUserRepo, error) {
	client, err := db.NewMongoClient(os.Getenv("MONGO_URI"))
	if err != nil {
		return ConcreteUserRepo{}, err
	}

	coll, err1 := db.NewMongoColl(client, "user")
	if err1 != nil {
		return ConcreteUserRepo{}, err
	}

	return ConcreteUserRepo{
		client:   client,
		userColl: coll,
	}, nil
}

func (c *ConcreteUserRepo) NewRecord(name, email, pass string) error {
	if c.IsExist(name) {
		return errors.New("Failed: record already exists")
	}

	_, err := c.userColl.InsertOne(
		context.Background(),
		bson.D{
			{"name", name},
			{"email", email},
			{"pass", pass},
		},
	)

	if err != nil {
		return errors.New("Failed to insert")
	}
	return nil
}

func (c *ConcreteUserRepo) IsExist(name string) bool {
	ctx := context.Background()
	cursor, err := c.userColl.Find(ctx,
		bson.D{{"name", name}})

	var items []bson.D
	if err = cursor.All(ctx, &items); err != nil {
		return false
	}

	if len(items) == 0 {
		return false
	}
	return true
}

func (c *ConcreteUserRepo) IsValid(name, pass string) bool {
	ctx := context.Background()
	cursor, err := c.userColl.Find(ctx,
		bson.D{{"name", name}})

	if err != nil {
		return false
	}

	var items []bson.M
	cursor.All(ctx, &items)

	obj := items[0]

	var x model.User
	bsonBytes, _ := bson.Marshal(obj)
	bson.Unmarshal(bsonBytes, &x)

	if string(x.Password) != pass {
		return false
	}
	return true
}

func (c *ConcreteUserRepo) Delete(name string) error {
	if !c.IsExist(name) {
		return errors.New("Record not exist")
	}

	_, err := c.userColl.DeleteOne(context.Background(), bson.D{{"name", name}})
	return err
}
