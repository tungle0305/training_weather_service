package repo

import (
	"log"
	"shopx/training/weatherApp/model"
	"shopx/training/weatherApp/services/util"
	"testing"
)

var Repo ConcreteForecastRepo

func init() {
	log.Println("Hello")
	util.SetOSEnv()
	Repo, _ = NewForecastRepo()
}

func testInitRepo(t *testing.T) {
	_, err := NewForecastRepo()

	if err != nil {
		t.Errorf("fail to init user repo %v", err)
	}

}

func testNewRecord(t *testing.T) {
	if err := Repo.NewRecord(model.Forecast{City: "Hanoi"}); err != nil {
		t.Errorf("Fail to add new record")
	}

	if err := Repo.NewRecord(model.Forecast{City: "Hanoi"}); err == nil {
		t.Errorf("Fail: allow add duplicated record")
	}
}

func testIsExist(t *testing.T) {
	Repo.NewRecord(model.Forecast{City: "HCM"})

	if !Repo.IsExist("HCM") {
		t.Errorf("Fail to check existed record")
	}

	if Repo.IsExist("GiaLai") {
		t.Errorf("Non-existed record found at IsExist()")
	}
}

func testGetByCity(t *testing.T) {
	city := "HCM"
	ans, err := Repo.GetByCity(city)

	if err != nil {
		t.Errorf("Fail to retrieve existed record")
	}

	city = "BinhChau"
	ans, err = Repo.GetByCity(city)

	if err == nil {
		t.Errorf("Fail: record not existed, yet still retrieve - err %v", err)
	}
	log.Println(ans)
}

func TestDelete(t *testing.T) {
	city := "HCM"
	err := Repo.Delete(city)

	if err != nil {
		t.Errorf("Fail to delete existed record")
	}

	city = "BinhChau"
	err = Repo.Delete(city)

	if err == nil {
		t.Errorf("Fail: record not existed, yet still proceed delete")
	}
}
