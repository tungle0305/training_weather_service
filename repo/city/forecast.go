package repo

import (
	"context"
	"errors"
	"os"
	"shopx/training/weatherApp/model"
	"shopx/training/weatherApp/services/db"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type ConcreteForecastRepo struct {
	ctx         context.Context
	client      *mongo.Client
	weatherColl *mongo.Collection
}

func NewForecastRepo() (ConcreteForecastRepo, error) {
	client, err := db.NewMongoClient(os.Getenv("MONGO_URI"))
	if err != nil {
		return ConcreteForecastRepo{}, err
	}

	coll, err1 := db.NewMongoColl(client, "forecast")
	if err1 != nil {
		return ConcreteForecastRepo{}, err
	}

	return ConcreteForecastRepo{
		ctx:         context.Background(),
		client:      client,
		weatherColl: coll,
	}, nil
}

func (c *ConcreteForecastRepo) GetByCity(city string) (model.Forecast, error) {
	if !c.IsExist(city) {
		return model.Forecast{}, errors.New("weather data not found")
	}

	ctx := context.Background()
	cursor, err := c.weatherColl.Find(ctx,
		bson.D{{"city", city}})

	if err != nil {
		return model.Forecast{}, errors.New("Error when retrieving data")
	}

	var items []bson.M
	if err = cursor.All(ctx, &items); err != nil {
		return model.Forecast{}, errors.New("weather data not found")
	}

	obj := items[0]

	var x model.Forecast
	bsonBytes, _ := bson.Marshal(obj)
	bson.Unmarshal(bsonBytes, &x)

	return x, nil
}

func (c *ConcreteForecastRepo) NewRecord(data model.Forecast) error {
	if c.IsExist(data.City) {
		return errors.New("Record already existed")
	}

	_, err := c.weatherColl.InsertOne(
		context.Background(),
		bson.D{
			{"city", data.City},
			{"main", data.Main},
			{"temp", data.Temp},
			{"temp_min", data.TempMin},
			{"temp_max", data.TempMax},
			{"pressure", data.Pressure},
			{"humidity", data.Humidity},
			{"wind_speed", data.WindSpeed},
		},
	)

	if err != nil {
		return errors.New("Failed to insert")
	}
	return nil
}

func (c *ConcreteForecastRepo) IsExist(city string) bool {
	ctx := context.Background()
	cursor, err := c.weatherColl.Find(ctx,
		bson.D{{"city", city}})

	var items []bson.D
	if err = cursor.All(ctx, &items); err != nil {
		return false
	}

	if len(items) == 0 {
		return false
	}
	return true
}

func (c *ConcreteForecastRepo) Delete(city string) error {
	if !c.IsExist(city) {
		return errors.New("Record not exist")
	}

	_, err := c.weatherColl.DeleteOne(context.Background(), bson.D{{"city", city}})
	return err
}
